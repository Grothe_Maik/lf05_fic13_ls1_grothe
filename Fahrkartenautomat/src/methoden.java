import java.util.Scanner;

class methoden
{
	
	public static double fahrkartenbestellungErfassen() { //erfassen der Tickets, Berechnung Betrag
		double zwischenSumme=0;
		double zuZahlenderBetrag=0; 
		byte anzahlTickets; 
		char weiter;
		String[] tickets = new String[] {
			 	"[1]  Einzelfahrschein Berlin AB:                         2.90 �\n",//musste ',' durch '.' ersetzen f�r string to float conv.
			 	"[2]  Einzelfahrschein Berlin BC:                         3.30 �\n", //: als erkennung f�r index
			 	"[3]  Einzelfahrschein Berlin ABC:                        3.60 �\n",
			 	"[4]  Kurzstrecke:                                        1.90 �\n",
			 	"[5]  Tageskarte Berlin AB:                               8.60 �\n",
			 	"[6]  Tageskarte Berlin BC:                               9.00 �\n",
			 	"[7]  Tageskarte Berlin ABC:                              9.60 �\n",
			 	"[8]  Kleingruppen-Tageskarte Berlin AB:                 23.50 �\n",
			 	"[9]  Kleingruppen-Tageskarte Berlin BC:                 24.30 �\n",
			 	"[10] Kleingruppen-Tageskarte Berlin ABC:                24.90 �\n",
			
			 	
		};
		float[] preise = new float [tickets.length];
		for (int i = 0; i <= tickets.length-1; i++) {
			preise[i] = Float.parseFloat(tickets[i].substring(tickets[i].indexOf(":")+1, tickets[i].indexOf("�"))); // theoretisch zweites Array unn�tig
		}
		
		
		do {
			for (int i = 0;  i <= tickets.length-1; i++ ) {
				System.out.print(tickets[i]); //bessere Ausgabe mit printf... sp�ter
			}
			System.out.print("\nW�hlen Sie ein Ticket [1] bis [" + tickets.length + "]: " );
			Scanner tastatur = new Scanner(System.in);
			int auswahl = tastatur.nextInt()-1;
			
		 // System.out.print("Zu zahlender Betrag (EURO): ");
		 // Scanner tastatur = new Scanner(System.in);
		 // zuZahlenderBetrag = tastatur.nextDouble();
	       System.out.print("Anzahl der Tickets: ");
	       anzahlTickets = tastatur.nextByte();
	       
	       if ((anzahlTickets > 10 || anzahlTickets < 1)) {
	    	   System.out.print("W�hlen Sie eine Ticketanzahl zwischen 1 und 10\n \n\n");
	    	    }
	       zwischenSumme = zwischenSumme + (anzahlTickets * preise[auswahl]);
	       System.out.print("Zwischensumme:" );
	       System.out.printf("%.2f", zwischenSumme );
	       System.out.print(" �. \nF�r weitere Tickets 'Weiter' [w] eingeben: ");
	      weiter = tastatur.next().charAt(0);
		}while ((anzahlTickets > 10 || anzahlTickets < 1)||(zwischenSumme < 0)||(weiter == 'w'));
	     
	     //  zuZahlenderBetrag= zuZahlenderBetrag*anzahlTickets; //der Wert zuZahlenderBetrag wird mit anzahlTickets multi. und zuZahlenderBEtrag wieder zugewiesen.
	       return zwischenSumme;
	}
	
	public static double fahrkartenBezahlen(double zuZahlen) { //Geldeinwurf
		Scanner tastatur =  new Scanner(System.in);
		double eingezahlterGesamtbetrag = 0.0;
		double zuZahlenderBetrag= zuZahlen;
		double eingeworfeneM�nze;
	       while(eingezahlterGesamtbetrag < zuZahlenderBetrag)
	       {
	    	   System.out.print("Noch zu zahlen: ");
	    	   System.out.printf("%.2f", (zuZahlenderBetrag - eingezahlterGesamtbetrag));  //�nderung: neue Zeile, Formatierung der Gleitkommazahl
	    	   System.out.print(" Euro.\nEingabe (mind. 5Ct, h�chstens 2 Euro): ");             //neue Zeile f�r eingabe
	    	   eingeworfeneM�nze = tastatur.nextDouble();
	           eingezahlterGesamtbetrag += eingeworfeneM�nze;
	         
	       }

		return eingezahlterGesamtbetrag;
	}
	
	public static void fahrkartenAusgeben() {// Fahrscheinausgabe
	     System.out.println("\nFahrschein wird ausgegeben");
	       for (int i = 0; i < 8; i++)
	       {
	          System.out.print("=");
	          try {
				Thread.sleep(250);
			} catch (InterruptedException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
	       }
	       System.out.println("\n\n");
	       return;
	}
	
	public static void rueckgeldAusgeben(double eingezahlterGesamtbetrag, double zuZahlen) {// R�ckgeldberechnung und -Ausgabe
		
		double  r�ckgabebetrag = 100*(eingezahlterGesamtbetrag - zuZahlen);
	       while(r�ckgabebetrag > 0.0)
	       {
	    	   System.out.printf("Der R�ckgabebetrag in H�he von %.2f",(r�ckgabebetrag)/100 ); //�nderung: Formatierung des R�ckgelds "Euro" in n�chstes print
	    	   System.out.print(" Euro. \n");
	    	   System.out.println("wird in folgenden M�nzen ausgezahlt:");

	           while(r�ckgabebetrag >= 200.0) // 2 EURO-M�nzen
	           {
	        	  System.out.println("2 EURO");
		          r�ckgabebetrag -= 200.0;
	           }
	           while(r�ckgabebetrag >= 100.0) // 1 EURO-M�nzen
	           {
	        	  System.out.println("1 EURO");
		          r�ckgabebetrag -= 100.0;
	           }
	           while(r�ckgabebetrag >= 50.0) // 50 CENT-M�nzen
	           {
	        	  System.out.println("50 CENT");
		          r�ckgabebetrag -= 50.0;
	           }
	           while(r�ckgabebetrag >= 20.0) // 20 CENT-M�nzen
	           {
	        	  System.out.println("20 CENT");
	 	          r�ckgabebetrag -= 20.0;
	           }
	           while(r�ckgabebetrag >= 10.0) // 10 CENT-M�nzen
	           {
	        	  System.out.println("10 CENT");
		          r�ckgabebetrag -= 10.0;
	           }
	           while(r�ckgabebetrag >= 5.0)// 5 CENT-M�nzen
	           {
	        	  System.out.println("5 CENT");
	 	          r�ckgabebetrag -= 5.0;
	           }
	           while(r�ckgabebetrag > 0.00)// 1 CENT-M�nzen
	           {
	        	  System.out.println("1 CENT");
	 	          r�ckgabebetrag -= 1.0;
	           }
	       }
       return ;
	}
	
	public static int verkauf() {
		System.out.print("Wenn Sie weitere Tickets erwerben m�chten dr�cken sie auf \'Weiter\'(w): ");
		Scanner tastatur =  new Scanner(System.in);
		char weiter= tastatur.next().charAt(0);
		
		
		if (weiter == 'w' ) {
			System.out.print("\n---Neuer Vorgang---\n\n");
		return 1;
		
		}
		else {
			System.out.print("Auf Wiedersehen.\n");
		}
		return 0;
	}
	
    public static void main(String[] args)
    {

      int naechster;
	do {
       double zuZahlen=fahrkartenbestellungErfassen();
       double eingezahlterGesamtbetrag = fahrkartenBezahlen(zuZahlen);
       fahrkartenAusgeben();
       rueckgeldAusgeben(eingezahlterGesamtbetrag, zuZahlen);
       
       
       
       System.out.println("\nVergessen Sie nicht, den Fahrschein\n"+
                          "vor Fahrtantritt entwerten zu lassen!\n"+
                          "Wir w�nschen Ihnen eine gute Fahrt.\n");
       naechster = verkauf();
       
      } while(naechster == 1);
    }
}
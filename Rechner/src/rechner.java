import java.util.Scanner; // Import der Klasse Scanner 
 
public class rechner  
{ 
   
  public static void main(String[] args) // Hier startet das Programm 
  { 
     
    // Neues Scanner-Objekt myScanner wird erstellt     
    Scanner myScanner = new Scanner(System.in);  
     
    System.out.print("Bitte geben Sie eine ganze Zahl ein: ");    
     
    // Die Variable zahl1 speichert die erste Eingabe 
    double zahl1 = myScanner.nextInt();  
     
    System.out.print("Bitte geben Sie eine zweite ganze Zahl ein: "); 
     
    // Die Variable zahl2 speichert die zweite Eingabe 
    double zahl2 = myScanner.nextInt();  
     
    // Die Addition der Variablen zahl1 und zahl2  
    // wird der Variable ergebnis zugewiesen. 
    double ergebnis = zahl1 + zahl2;  
    double ergebnissub = zahl1 - zahl2;
    double ergebnisdiv = zahl1 / zahl2;
    double ergebnismul = zahl1*zahl2;
    
    System.out.print("\n\n\nErgebnis der Addition lautet: "); 
    System.out.print(zahl1 + " + " + zahl2 + " = " + ergebnis);   
 
    System.out.print("\n\nErgebnis der Subtraktion lautet: ");
    System.out.print(zahl1 + " - " + zahl2 + " = " + ergebnissub);
    
    System.out.print("\n\nErgebnis der Division lautet: ");
    System.out.printf(zahl1 + " : " + zahl2 + " = "  + "%.2f" , ergebnisdiv);
    
    System.out.print("\n\nErgebnis der Multiplikation lautet: ");
    System.out.print(zahl1 + " * " + zahl2 + " = " + ergebnismul);
    
    myScanner.close(); 
     
  }    
}
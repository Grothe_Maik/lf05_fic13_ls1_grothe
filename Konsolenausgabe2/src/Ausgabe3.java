
public class Ausgabe3 {

	public static void main (String[] arg) {
		double c1 = -28.8889;
		double c2 = -23.3333 ;
		double c3 = -17.7778 ;
		double c4 = -6.6667 ;
		double c5 = -1.1111 ;
		
		int d1    = 20;
		int d2    = 10;
		int d3    = 0;
		int d4    = 20;
		int d5    = 30;
		
		
		System.out.printf( "%-12s",  "Fahrenheit" );
		System.out.print("|");
		System.out.printf( " %10s",  "Celsius\n" );
		System.out.print ("-----------------------\n");
		
		System.out.printf( "%-12d",  -d1 );
		System.out.print("|");
		System.out.printf( "%10.2f\n",  c1 );
		
		System.out.printf( "%-12d",  -d2 );
		System.out.print("|");
		System.out.printf( "%10.2f\n",  c2 );
		
		System.out.printf( "+%-11d",  d3 );
		System.out.print("|");
		System.out.printf( "%10.2f\n",  c3 );

		System.out.printf( "+%-11d",  d4 );
		System.out.print("|");
		System.out.printf( "%10.2f\n",  c4 );
		
		System.out.printf( "+%-11d",  d5 );
		System.out.print("|");
		System.out.printf( "%10.2f\n",  c5 );





	}
}


public class Ausgabe2 {
	
	public static void main (String[] arg) {
		
		System.out.printf( " 0!%-5s=",  "" );
		System.out.printf( " %-19s=",  "" );
		System.out.printf( " %4s",  "1\n" );
		
		System.out.printf( " 1!%-5s=",  "" );
		System.out.printf( " %-19s=",  "1" );
		System.out.printf( " %4s",  "1\n" );
		
		System.out.printf( " 2!%-5s=",  "" );
		System.out.printf( " %-19s=",  "1 * 2" );
		System.out.printf( " %4s",  "2\n" );

		System.out.printf( " 3!%-5s=",  "" );
		System.out.printf( " %-19s=",  "1 * 2 * 3" );
		System.out.printf( " %4s",  "6\n" );
		
		System.out.printf( " 4!%-5s=",  "" );
		System.out.printf( " %-19s=",  "1 * 2 * 3 * 4" );
		System.out.printf( " %4s",  "24\n" );
		
		System.out.printf( " 5!%-5s=",  "" );
		System.out.printf( " %-19s=",  "1 * 2 * 3 * 4 * 5" );
		System.out.printf( " %4s",  "120\n" );





	}
}
